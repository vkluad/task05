#include "../include/converter.h"

struct proc_ops proc_fops = { .proc_read = read_procfs,
			     .proc_write = write_procfs };

char *proc_buffer;
struct proc_dir_entry *proc_dir;
struct proc_dir_entry *proc_file;
size_t procfs_buffer_size = 0;

int create_buffer(void)
{
	proc_buffer = kmalloc(BUFFER_SIZE, GFP_KERNEL);
	if (proc_buffer == NULL)
		return -ENOMEM;
	strncpy(proc_buffer, "\000", BUFFER_SIZE);
	procfs_buffer_size = 0;
	return 0;
}

void cleanup_buffer(void)
{
	if (proc_buffer) {
		kfree(proc_buffer);
		proc_buffer = NULL;
	}
	procfs_buffer_size = 0;
}

int create_proc(void)
{
	proc_dir = proc_mkdir(MODULE_TAG, NULL);
	if (NULL == proc_dir)
		return -EFAULT;
	proc_file = proc_create(PROC_FILENAME, S_IFREG | S_IRUGO | S_IWUGO,
				proc_dir, &proc_fops);

	if (NULL == proc_file)
		return -EFAULT;

	return 0;
}

void cleanup_proc(void)
{
	if (proc_file) {
		remove_proc_entry(PROC_FILENAME, proc_dir);
		proc_file = NULL;
	}

	if (proc_dir) {
		remove_proc_entry(MODULE_TAG, NULL);
		proc_dir = NULL;
	}
}


ssize_t read_procfs(struct file *file_p, char __user *buffer,
			   size_t length, loff_t *offset)
{
	size_t left;

	if (*offset != 0)
		return 0;

	rem_dot(&proc_buffer[*offset]);
	convert_currency_to_currency(&proc_buffer[*offset]);

	length = strlen(proc_buffer);

	left = copy_to_user(buffer, &proc_buffer[*offset], length);

	printk(KERN_NOTICE MODULE_TAG ": convert complated\n");

	procfs_buffer_size = 0;
	strncpy(proc_buffer, "\000", BUFFER_SIZE);

	*offset += length - left;

	return length - left;
}

ssize_t write_procfs(struct file *file_p, const char __user *buffer,
			    size_t length, loff_t *offset)
{
	size_t msg_length;
	size_t left;

	if (length > BUFFER_SIZE) {
		msg_length = BUFFER_SIZE;
	} else
		msg_length = length;

	strncpy(proc_buffer, "\000", BUFFER_SIZE);

	left = copy_from_user(proc_buffer, buffer, msg_length);

	*offset = 0;
	return length;
}

