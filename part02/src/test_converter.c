#include <linux/module.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Vlad Kudenchuk <vkluad@gmail.com>");
MODULE_VERSION("0.01");
MODULE_DESCRIPTION("Test converter Linux module.");

extern void rem_dot(char s[]);
extern void convert_currency_to_currency(char *from_to_value);

static char *test_convert_currency_to_currency_and_rem_dot(void)
{
	static char res[512], temp[80];

	strncpy(temp, "\000", 80);
	strcpy(temp, "12.35:EUR_to_UAH\n");
	rem_dot(temp);
	convert_currency_to_currency(temp);
	strncpy(res, "\000", 512);
	strncpy(res, "\n\n----TEST STARTED----\n", 24);

	if (strcmp(temp, "12.35 EUR it 398.28 UAH\n") == 0)
		strcat(res, "\nConvert 12.35 EUR to UAH: PASSED\n");
	else
		strcat(res, "\nConvert 12.35 EUR to UAH: UNPASSED\n");

	strcat(res, temp);

	strncpy(temp, "\000", 80);
	strcpy(temp, "12.35:PLZ_to_EUR\n");
	rem_dot(temp);
	convert_currency_to_currency(temp);

	if (strcmp(temp, "12.35 PLZ it 2.73 EUR\n") == 0)
		strcat(res, "\nConvert 12.35 EUR to PLZ: PASSED\n");
	else
		strcat(res, "\nConvert 12.35 EUR to PLZ: UNPASSED\n");

	strcat(res, temp);

	strncpy(temp, "\000", 80);
	strcpy(temp, "12.35:USD_to_EUR\n");
	rem_dot(temp);
	convert_currency_to_currency(temp);

	if (strcmp(temp, "12.35 EUR it 14.66 USD\n") == 0)
		strcat(res, "\nConvert 12.35 EUR to USD: PASSED\n");
	else
		strcat(res, "\nConvert 12.35 EUR to USD: UNPASSED\n");

	strcat(res, temp);
	strncat(res, "\n----TEST ENDED----\n\n", 23);

	return res;
}

static int __init tinit(void)
{
	printk(KERN_INFO "test_task05: load\n");
	printk(KERN_INFO "test_task05: %s",
	       test_convert_currency_to_currency_and_rem_dot());
	printk(KERN_INFO "test_task05: exit...\n");
	return -1;
}

module_init(tinit);

