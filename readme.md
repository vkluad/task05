# TASK 05

## Description

Currency converter.

---

## References

- [procfs demo](https://gitlab.com/gl-khpi/examples/tree/master/procfs_rw).
- [sysfs demo](https://gitlab.com/gl-khpi/examples/tree/master/sysfs).

---

## Guidance

> Use [Git](https://gl-khpi.gitlab.io/#commit-requirements) and the following names: `task05` - for your project's home directory; `partXX` - for the directory of each subtask (`XX` - subtask number); `src` - for the source code directory.

:one: **Subtask #1**. Implement the currency converter as a kernel module to...

- convert currency from/to Ukrainian hryvnia to/from euro. :ballot_box_with_check:
- set the conversion factor based on the hryvnia. :ballot_box_with_check:

:two: **Subtask #2**. Add the interfaces into the module:

- *procfs* for currency conversion. :ballot_box_with_check:
- *sysfs* to set conversion factors corresponding to the currencies used. :ballot_box_with_check:

:heart: Implement bash script for the module testing. :ballot_box_with_check:

:yellow_heart: Develop a C application to test module functionality. :ballot_box_with_check:

:green_heart: Provide a test scenario. :ballot_box_with_check:

---

## Extra

Add the following functionality to the kernel module:

- conversion logging; :ballot_box_with_check:
- dynamically add a new type of currency for conversion. :ballot_box_with_check:

---

# Results:

1. Test scenario bash:

![Image01](res/image01.png)

2. `dmesg` output:

![Image02](res/image02.png)

