#ifndef __CONVERTER_H_TASK05
#define __CONVERTER_H_TASK05

#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/slab.h>
#include <asm/uaccess.h>
#include <linux/proc_fs.h>
#include <linux/kernel.h>
#include <linux/kern_levels.h>
#include <linux/module.h>


#define MODULE_TAG "task05"
extern long l_exchange_rate;

/* string func */
extern void rem_dot(char s[]);
extern void convert_currency_to_currency(char *from_to_value);
/* end string */

/* procfs func */

#define PROC_FILENAME "converter"
#define BUFFER_SIZE 128

extern char *proc_buffer;
extern struct proc_dir_entry *proc_dir;
extern struct proc_dir_entry *proc_file;
extern size_t procfs_buffer_size;

extern ssize_t read_procfs(struct file *file_p, char __user *buffer,
			   size_t length, loff_t *offset);
extern ssize_t write_procfs(struct file *file_p, const char __user *buffer,
			    size_t length, loff_t *offset);
extern int create_buffer(void);
extern void cleanup_buffer(void);
extern int create_proc(void);
extern void cleanup_proc(void);

extern struct proc_ops proc_fops;

/* end procfs */

/* sysfs func*/

#define SYSFS_BUF_SIZE 1024

extern char list_of_currency[SYSFS_BUF_SIZE + 1];

extern ssize_t exchange_rate_show(struct class *class,
				  struct class_attribute *attr, char *buf);

extern ssize_t exchange_rate_store(struct class *class,
				   struct class_attribute *attr,
				   const char *buf, size_t count);

extern struct class *exchange_rate_class;

/* end sysfs */

#endif
