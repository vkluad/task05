#include "../include/converter.h"

char list_of_currency[SYSFS_BUF_SIZE + 1] = "";

ssize_t exchange_rate_show(struct class *class,
				  struct class_attribute *attr, char *buf)
{
	strncpy(buf, list_of_currency, strlen(list_of_currency));
	printk(KERN_NOTICE MODULE_TAG ": show currency list:\n%s", buf);
	return strlen(buf);
}

ssize_t exchange_rate_store(struct class *class,
				   struct class_attribute *attr,
				   const char *buf, size_t count)
{
	char *temp_buf = kstrdup(buf, GFP_KERNEL), *temp, *start_buf = temp_buf;

	printk(KERN_NOTICE MODULE_TAG ": write to currency list: %s", temp_buf);

	temp = strsep(&temp_buf, ".");
	if (temp)
		strncat(list_of_currency, temp, strlen(temp));

	temp = strsep(&temp_buf, ".");
	if (temp)
		strncat(list_of_currency, temp, strlen(temp));

	kfree(start_buf);
	return count;
}

struct class *exchange_rate_class;

