# Part02
## How to use 

EUR to UAH : `echo "123.12:EUR_to_UAH" > /proc/task05/converter && cat /proc/task05/converter`

UAH to EUR : `echo "123.12:UAH_to_EUR" > /proc/task05/converter && cat /proc/task05/converter`

etc.
## Set currency
`echo "currency_in_UAH:32.00" > /sys/class/exchange_rate/exchange_rate`
	
## Test this module
`make all && ./test`
	
## Show work of module
`make all && ./show`

