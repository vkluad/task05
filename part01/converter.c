#include <linux/module.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("vkluad");
MODULE_VERSION("0.01");
MODULE_DESCRIPTION("converter");

const int UAH_in_EUR = 3215;

void reverse(char s[])
 {
     int i, j;
     char c;

     for (i = 0, j = strlen(s)-1; i<j; i++, j--) {
         c = s[i];
         s[i] = s[j];
         s[j] = c;
     }
 }

void itoa(int n, char s[])
 {
     int i, sign;
     if ((sign = n) < 0)
         n = -n;
     i = 0;
     do {
         s[i++] = n % 10 + '0';
     } while ((n /= 10) > 0);
     if (sign < 0)
         s[i++] = '-';
     s[i] = '\0';
     reverse(s);
 }

char *convert_EUR_to_UAH(const int EUR) /* EUR in fixed point format, just multiplied by 100. Example 32.15 this 3215 in fixed point format */
{
	int UAH = EUR * UAH_in_EUR / 100;
        static char res[80];
	char UAH_s[10], EUR_s[10];

	itoa(EUR / 100, EUR_s);
	strcpy(res, EUR_s);
	strcat(res, ".");
	itoa(EUR % 100, EUR_s);
	strcat(res, EUR_s);
	strcat(res, " EUR it ");

	itoa(UAH / 100, UAH_s);
	strcat(res, UAH_s);
	strcat(res, ".");
	itoa(UAH % 100, UAH_s);
	strcat(res, UAH_s);

	strcat(res, " in UAH");
    return res;
}

char *convert_UAH_to_EUR(const int UAH) /* UAH in fixed point format, just multiplied by 100. Example 32.15 this 3215 in fixed point format */
{
	int EUR = UAH * 100 / UAH_in_EUR;
        static char res[80];
	char UAH_s[10], EUR_s[10];

	itoa(UAH / 100, UAH_s);

	strcpy(res, UAH_s);
	strcat(res,".");
	itoa(UAH % 100, UAH_s);
	strcat(res, UAH_s);

	strcat(res, " UAH it ");

	itoa(EUR / 100, EUR_s);
	strcat(res, EUR_s);
	strcat(res,".");
	itoa(EUR % 100, EUR_s);
	strcat(res, EUR_s);

	strcat(res, " in EUR");
    return res;
}

static int __init xinit(void)
{
    printk(KERN_INFO "converter: %s\n", convert_EUR_to_UAH(12125));
    printk(KERN_INFO "converter: %s\n", convert_UAH_to_EUR(313018));
    return 0;
}

static void __exit xexit(void)
{
}

module_init(xinit);
module_exit(xexit);
