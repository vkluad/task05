#include "../include/converter.h"

void reverse(char s[])
{
	int i, j;
	char c;

	if (s == NULL)
		return;

	for (i = 0, j = strlen(s) - 1; i < j; i++, j--) {
		c = s[i];
		s[i] = s[j];
		s[j] = c;
	}
}

void itoa(int n, char s[])
{
	int i, sign;

	if (s == NULL)
		return;

	if ((sign = n) < 0)
		n = -n;
	i = 0;
	do {
		s[i++] = n % 10 + '0';
	} while ((n /= 10) > 0);
	if (sign < 0)
		s[i++] = '-';
	s[i] = '\0';
	reverse(s);
}

void rem_dot(char s[])
{
	char *string = kstrdup(s, GFP_KERNEL), *start_string = string, *toks;

	strncpy(s, "", strlen(s));
	while ((toks = strsep(&string, ".\n")) != NULL) {
		strcat(s, toks);
	}
	s[strlen(s)] = '\n';

	kfree(start_string);
}
EXPORT_SYMBOL(rem_dot);

#define find_currency(find_value)                                              \
	while ((s_exchange_rate = strsep(&buffer, "\n")) != NULL) {            \
		currency_find_status = 0;                                      \
		if (strncmp(s_exchange_rate, (find_value), 3) == 0) {          \
			strsep(&s_exchange_rate, ":\n");                       \
			if (kstrtol(strsep(&s_exchange_rate, ":\n"), 10,       \
				    &l_exchange_rate) != 0) {                  \
				printk(KERN_ERR MODULE_TAG                     \
				       ": don`t have a number in sysfs\n");    \
				goto line_form;                                \
			}                                                      \
			currency_find_status = 1;                              \
			break;                                                 \
		}                                                              \
	}                                                                      \
	if (!currency_find_status) {                                           \
		printk(KERN_ERR MODULE_TAG ": Unknown currency!!!\n");         \
		strncpy(from_to_value, "Error: Unknown currency!!!\n", 27);    \
		goto free;                                                     \
	}

void convert_currency_to_currency(char *from_to_value)
{
	int value = 0, current_value = 0;
	char *s_exchange_rate = NULL, from_value[10] = "", to_value[10] = "",
	     from_value_DIV[3] = "", to_value_DIV[3] = "",
	     *buffer = kstrdup(list_of_currency, GFP_KERNEL),
	     *string = kstrdup(from_to_value, GFP_KERNEL), *from = NULL,
	     *to = NULL, *start_buf = buffer, *start_string = string;
	_Bool currency_find_status = 0;

	if (from_to_value == NULL)
		goto free;

	if (kstrtol(strsep(&string, ":_\n"), 10, (long *)&value) != 0) {
		printk(KERN_ERR MODULE_TAG ": don`t have a number\n");
		goto line_form;
	}

	current_value = value;

	from = strsep(&string, ":_\n");
	strsep(&string, "_\n");
	to = strsep(&string, "_\n");

	printk(KERN_NOTICE MODULE_TAG ": convert from %s to %s", from, to);

	find_currency(from);

	if (strcmp(from, "UAH") != 0)
		current_value = value * l_exchange_rate / 100;

	if ((strcmp(to, "UAH") == 0))
		goto line_form;

	buffer = start_buf;

	find_currency(to);

	current_value = current_value * 100 / l_exchange_rate;

line_form:
	if ((from == NULL) || (to == NULL))
		goto free;

	itoa(value / 100, from_value);
	itoa(value % 100, from_value_DIV);
	itoa(current_value / 100, to_value);
	itoa(current_value % 100, to_value_DIV);

	strncpy(from_to_value, "\000", BUFFER_SIZE);
	strncat(from_to_value, from_value, strlen(from_value));
	strncat(from_to_value, ".", 1);
	strncat(from_to_value, from_value_DIV, 2);
	strncat(from_to_value, " ", 1);
	strncat(from_to_value, from, strlen(from));

	strncat(from_to_value, " it ", 4);

	strncat(from_to_value, to_value, strlen(to_value));
	strncat(from_to_value, ".", 1);
	strncat(from_to_value, to_value_DIV, 2);
	strncat(from_to_value, " ", 1);

	strncat(from_to_value, to, strlen(to));

	strncat(from_to_value, "\n", 1);

	printk(KERN_NOTICE MODULE_TAG ": converted: %s", from_to_value);

free:

	if (start_buf)
		kfree(start_buf);

	if (start_string)
		kfree(start_string);

	buffer = NULL;
	start_buf = NULL;
	string = NULL;
	start_string = NULL;
}
EXPORT_SYMBOL(convert_currency_to_currency);

