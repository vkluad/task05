#include "../include/converter.h"

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Vlad Kudenchuk <vkluad@gmail.com>");
MODULE_VERSION("0.01");
MODULE_DESCRIPTION("-_-Currency converter-_-");

long l_exchange_rate = 3215; /* fixed point format */


CLASS_ATTR_RW(exchange_rate);

static int __init task05_init(void)
{
	int err, res;
	err = create_buffer();

	if (err)
		goto error;

	err = create_proc();
	if (err)
		goto error;

	printk(KERN_NOTICE MODULE_TAG ": loaded\n");
	exchange_rate_class = class_create(THIS_MODULE, "exchange_rate");

	if (IS_ERR(exchange_rate_class))
		printk(KERN_ERR MODULE_TAG ": bad class sysfs create\n");

	res = class_create_file(exchange_rate_class, &class_attr_exchange_rate);
	printk(KERN_NOTICE MODULE_TAG ": sysfs initialized\n");
	return res;

error:
	printk(KERN_ERR MODULE_TAG ": failed to load procfs\n");
	cleanup_proc();
	cleanup_buffer();
	return err;
}

static void __exit task05_exit(void)
{
	cleanup_proc();
	cleanup_buffer();

	class_remove_file(exchange_rate_class, &class_attr_exchange_rate);
	class_destroy(exchange_rate_class);

	printk(KERN_NOTICE MODULE_TAG ": exited\n");
}

module_init(task05_init);
module_exit(task05_exit);

